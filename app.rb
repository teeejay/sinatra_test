require 'bundler/setup'
Bundler.require # require all gems in gemfile!

# require 'rubygems'
require 'sinatra'
require 'sinatra/json'
require 'sinatra/base'

require './lib/review'

# TODO: Move to models/ dir
DataMapper.setup(:default, 'sqlite:memory')
DataMapper.finalize
DataMapper.auto_migrate!

# TODO: test automatically!

class SinatraTest < Sinatra::Base
  get '/' do
    @name = 'Thomas'
    "Hello #{@name}"
    # "Hello " + @name
  end

  get '/double' do
    erb :double
  end

  post '/double' do
    "Result: #{params[:number].to_f * 2}"
  end

  get '/reviews' do
    content_type :json

    reviews = Review.all
    reviews.to_json
  end

  get '/reviews/:id' do
    content_type :json

    review = Review.get params[:id]
    review.to_json
  end

  post '/reviews' do
    review = Review.new params[:review]
    if review.save
      status 201
    else
      status 500
      json review.errors.full_messages
    end
  end

  put '/reviews/:id' do
    review = Review.get params[:id]
    if review.update params[:review]
      status 200
      json 'Review was updated!'
    else
      status 500
      json review.errors.full_messages
    end
  end

  delete '/reviews/:id' do
    review = Review.get params[:id]
    if review.destroy
      status 200
      json 'Review removed'
    else
      status 500
      json 'Problem removing review'
    end
  end
  run! if __FILE__ == $PROGRAM_NAME
end
