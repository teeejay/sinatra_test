require 'rspec'

require_relative '../app.rb'
require 'rack/test'

set :environment, :test

describe 'My Test' do
  include Rack::Test::Methods

  def app
    SinatraTest
  end

  it 'should load hello world' do
    get '/'
    expect(last_response.status).to eq(200)
  end

  it 'should not load hello world on wrong page' do
    get '/wrong'
    expect(last_response.status).to eq(404)
  end

  it 'should return empty reviews when no reviews added' do
    get '/reviews'
    expect(last_response.body).to eq('[]')
  end

  it 'should return one review after posting' do
    # h = { 'Content-Type' => 'application/json' }
    review = { :name => 'Hello', :text => 'you' }
    body = { :review => review }
    post '/reviews', body

    expect(last_response.status).to eq(201)

    get '/reviews'
    hash = JSON.parse(last_response.body)

    expect(hash.length).to eq(1)
    expect(hash[0]['id']).to eq(1)
    expect(hash[0]['name']).to eq('Hello')
  end
end
